let rotate360 = [
    { transform: 'rotateY(360deg)' }
];
let slowInfinite = {
    duration: 1000,
    iterations: 1
}

function rotateImageIn(id){
    
    let test = document.getElementById(id);

    test.animate( rotate360, slowInfinite );
    test.style.filter = "grayscale(100%)";
    
}

function rotateImageOut(id){
    
    let test = document.getElementById(id);

    test.animate( rotate360, slowInfinite );
    test.style.filter = "grayscale(0%)";
    
}